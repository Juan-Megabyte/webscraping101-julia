# WebScraping101 - Julia

This project is a we scraping test using the Julia programming language

## Who is this project for?

This project was mostly done to learn about the Julia language and it's packages, this information could be usefull for someone who also wants to learn Julia.

## How is the project organized?

This project was made using Pluto.jl and I will provide the code that was created for editing purposes and also an HTML file so that you may see the notebook that was created.
