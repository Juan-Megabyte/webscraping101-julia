### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ 7ac5546a-fa2c-4589-aec1-b75e5ae05d46
using HTTP, Gumbo, DataFrames

# ╔═╡ 0ccee0aa-08e1-481c-8ce9-7274317de28b
using VegaLite

# ╔═╡ 5d19678a-9e8d-11ec-0a86-f5436ecbfafb
md" # Welcome to web scraping 101!

This notebook is for detailing how to use Julia to scrape from websites, for this project I will be scraping from [Serebii](https://serebii.net/) where we will get Pokemon Unite Pokemon info.

"

# ╔═╡ 9cbff4ff-6142-4461-ae0b-f26749632475
md" ## Getting started
Before we start scraping data we have to know what our packages are, you can't go minning for gold without your trusty pickaxe.

**Packages**

- HTTP.jl
- Gumbo.jl

"

# ╔═╡ 31a4298e-954b-44b9-b339-ec91195507c0
r = HTTP.get("https://example.com/")

# ╔═╡ 515ba560-68e9-437b-8e7f-03c7f0a82aa6
md"Here what we did was produce a wall of text that includes all the code of the webpage, from the metadata to the multiple tags that compose the page. Let's parse out the header and keep the HTML"

# ╔═╡ d85071e1-588d-4418-bee9-05d74dd15c72
r_parsed = parsehtml(String(r.body))

# ╔═╡ a316bfc5-d6bc-4140-bb5f-9aefbec8327a
md"Much better but let's keep diggin down!"

# ╔═╡ e4fec98b-7336-4086-be8d-5fd283e61fdb
head = r_parsed.root[1]

# ╔═╡ 866b11e8-15df-43ef-972f-535d5d0d8f97
body = r_parsed.root[2]

# ╔═╡ e13aa492-b43f-4602-bbcf-609aa797072e
md"We were able to get either the head or the body of the HTML depending on our necesities. Let's go even further and get the H1 text"

# ╔═╡ f868a788-4e5e-4f3b-bc6e-1ea0d9947c7b
h1 = body[1][1]

# ╔═╡ 065b53b2-54c2-40dc-b8a7-7c40020c108c
h1[1].text 

# ╔═╡ 12d813dd-01d4-48e7-8f40-32ddccc92451
md"There... wasn't that easy. Now to apply this to a project!

## Pokemon Database
We want to get info from serebii.net into a readable format to be transfered to a database.

"

# ╔═╡ 00e4419f-5218-4004-935a-54fcf4c46324
pr = HTTP.get("https://serebii.net/pokemonunite/pokemon.shtml")

# ╔═╡ 831ec78b-2a33-498a-8f0d-76c5f3d80779
pr_parsed = parsehtml(String(pr.body))

# ╔═╡ 2a0c52cd-49f6-45de-ae0f-63cd53612294
poke_table = pr_parsed.root[2][3][5][2][6]

# ╔═╡ 83d9c4cf-87f6-4cb3-a6c3-71fb3c4ff41d
DF = DataFrames

# ╔═╡ 77f06251-61f5-43f1-b949-7ab9ba885bea
df = DF.DataFrame(Pokémon = "", Difficulty = "", Style = "", Role = "", Attack_Types = "")

# ╔═╡ 48ac13e2-9d0d-4807-931c-091b1e873c5d
for i in 2:33
	poke_array = []
	push!(poke_array, poke_table[1][i][2][1][1][1].text)
	for j in 3:6
		push!(poke_array, poke_table[1][i][j][1].text)
	end
	push!(df, vec(poke_array))
end

# ╔═╡ 8ca327d6-83e9-498a-a793-c62c20db5c87
md" Using this for loop we are able to parse out specifically the information that we need, the name of the Pokemon, Difficulty of use, Play Style, Role, and Attack Types
"

# ╔═╡ 1935bbd5-9235-4401-97ae-299aa5c01504
df

# ╔═╡ 99794994-da36-4646-afa0-66a5781c371c
md"What a beautiful table of data there is just one issue, we have an empty first row lets go ahead and fix that real quick."

# ╔═╡ 6a6ed95f-7cd3-40fe-a571-03d2eeb1a541
pokemon = df[df[!,:Pokémon].!="",:]

# ╔═╡ ccacf398-faad-47ec-8f86-5fef1bdd43ca
md" Here we parse out the rows that contain an empty space.
"

# ╔═╡ ff444df1-eeae-4d01-9824-60811a703408
md"## With data the possibilities are endless
Now that we have our data we are able to do whatever we want to it, be it storing it in a database for later use or creating a visualization. Let's use VegaLite to create some quick analytics.
"

# ╔═╡ 4cbeabe6-5121-4baf-9c33-66c7bdb3ad10
pokemon |>
@vlplot(
    height=250,
    width=500,
    :bar,
    x="Role:n",
    y="count()",
	color={
        :Role,
        scale={
            domain=["All-Rounder","Attacker","Defender","Speedster","Supporter"],
            range=["grey","#606C38","grey","grey","grey"]
        },
        legend=false
	},
)

# ╔═╡ 47f3ad51-5c3a-4524-96ae-28786971c294
md"Just counting each type of pokemon we can see that Attackers seem to get the most love while Speedster and Supporter seem to have less options for players."

# ╔═╡ 5e07bdef-b750-4f17-9f77-f09fa17ed0ad
pokemon |>
@vlplot(
	width=250,
	heith=500,
	:bar,
	x="Difficulty:n",
	y="count()",
	color={
		:Difficulty,
		scale={
			domain=["Expert", "Intermediate","Novice"],
			range=["#BC6C25"]
		},
		legend=false
	}
)

# ╔═╡ 24ef77f6-cb6b-49f9-acbb-e9f574db7f21
md"
NOTE: Current version of the page contains a spelling error. Here is the fix:
"

# ╔═╡ 85bf7690-25a6-436e-9795-cd2a600305af
unique(pokemon[!,2])

# ╔═╡ a6f29ff1-6b0d-41c1-9064-ed0bab3a4693
pokemon[13,2] = "Intermediate"

# ╔═╡ f0b123d3-5b1d-4e11-9da2-1686cb2bef05
md"
Looking at Pokemon in terms of difficulty we can observe that while there are more Intermediate Pokemon there does seem to be a balance; a Pokemon for every level of player.
"

# ╔═╡ c4f7fe77-5277-4ec2-8825-22c455741355
md"
## Summary

We learned about 2 libraries for webscraping, how to utilize these libraries to get the information that we want and also how to transform this information into something usefull for our purposes. I hope this tiny guide was usefull for anyone wanting to learn a bit about webscraping with Julia. The libraries are in the early stages but there is alot of potential.
"

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
Gumbo = "708ec375-b3d6-5a57-a7ce-8257bf98657a"
HTTP = "cd3eb016-35fb-5094-929b-558a96fad6f3"
VegaLite = "112f6efa-9a02-5b7d-90c0-432ed331239a"

[compat]
DataFrames = "~1.3.2"
Gumbo = "~0.8.0"
HTTP = "~0.9.17"
VegaLite = "~2.6.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractTrees]]
git-tree-sha1 = "03e0550477d86222521d254b741d470ba17ea0b5"
uuid = "1520ce14-60c1-5f80-bbc7-55ef81b5835c"
version = "0.3.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "96b0bc6c52df76506efc8a441c6cf1adcb1babc4"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.42.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f74e9d5388b8620b4cee35d4c5a618dd4dc547f4"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.3.0"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "cc70b17275652eb47bc9e5f81635981f13cea5c8"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.9.0"

[[deps.DataFrames]]
deps = ["Compat", "DataAPI", "Future", "InvertedIndices", "IteratorInterfaceExtensions", "LinearAlgebra", "Markdown", "Missings", "PooledArrays", "PrettyTables", "Printf", "REPL", "Reexport", "SortingAlgorithms", "Statistics", "TableTraits", "Tables", "Unicode"]
git-tree-sha1 = "ae02104e835f219b8930c7664b8012c93475c340"
uuid = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
version = "1.3.2"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.DataValues]]
deps = ["DataValueInterfaces", "Dates"]
git-tree-sha1 = "d88a19299eba280a6d062e135a43f00323ae70bf"
uuid = "e7dc6d0d-1eca-5fa6-8ad6-5aecde8b7ea5"
version = "0.4.13"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FileIO]]
deps = ["Pkg", "Requires", "UUIDs"]
git-tree-sha1 = "80ced645013a5dbdc52cf70329399c35ce007fae"
uuid = "5789e2e9-d7fb-5bc7-8068-2c6fae9b9549"
version = "1.13.0"

[[deps.FilePaths]]
deps = ["FilePathsBase", "MacroTools", "Reexport", "Requires"]
git-tree-sha1 = "919d9412dbf53a2e6fe74af62a73ceed0bce0629"
uuid = "8fc22ac5-c921-52a6-82fd-178b2807b824"
version = "0.8.3"

[[deps.FilePathsBase]]
deps = ["Compat", "Dates", "Mmap", "Printf", "Test", "UUIDs"]
git-tree-sha1 = "04d13bfa8ef11720c24e4d840c0033d145537df7"
uuid = "48062228-2e41-5def-b9a4-89aafe57970f"
version = "0.9.17"

[[deps.Formatting]]
deps = ["Printf"]
git-tree-sha1 = "8339d61043228fdd3eb658d86c926cb282ae72a8"
uuid = "59287772-0a20-5a39-b81b-1366585eb4c0"
version = "0.4.2"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.Gumbo]]
deps = ["AbstractTrees", "Gumbo_jll", "Libdl"]
git-tree-sha1 = "e711d08d896018037d6ff0ad4ebe675ca67119d4"
uuid = "708ec375-b3d6-5a57-a7ce-8257bf98657a"
version = "0.8.0"

[[deps.Gumbo_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "29070dee9df18d9565276d68a596854b1764aa38"
uuid = "528830af-5a63-567c-a44a-034ed33b8444"
version = "0.10.2+0"

[[deps.HTTP]]
deps = ["Base64", "Dates", "IniFile", "Logging", "MbedTLS", "NetworkOptions", "Sockets", "URIs"]
git-tree-sha1 = "0fa77022fe4b511826b39c894c90daf5fce3334a"
uuid = "cd3eb016-35fb-5094-929b-558a96fad6f3"
version = "0.9.17"

[[deps.IniFile]]
git-tree-sha1 = "f550e6e32074c939295eb5ea6de31849ac2c9625"
uuid = "83e8ac13-25f8-5344-8a64-a9f2b223428f"
version = "0.5.1"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InvertedIndices]]
git-tree-sha1 = "bee5f1ef5bf65df56bdd2e40447590b272a5471f"
uuid = "41ab1584-1d38-5bbf-9106-f11c6c58b48f"
version = "1.1.0"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "abc9885a7ca2052a736a600f7fa66209f96506e1"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.4.1"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.JSONSchema]]
deps = ["HTTP", "JSON", "URIs"]
git-tree-sha1 = "2f49f7f86762a0fbbeef84912265a1ae61c4ef80"
uuid = "7d188eb4-7ad8-530c-ae41-71a32a6d4692"
version = "0.3.4"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS]]
deps = ["Dates", "MbedTLS_jll", "Random", "Sockets"]
git-tree-sha1 = "1c38e51c3d08ef2278062ebceade0e46cefc96fe"
uuid = "739be429-bea8-5141-9913-cc70e7f3736d"
version = "1.0.3"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.NodeJS]]
deps = ["Pkg"]
git-tree-sha1 = "905224bbdd4b555c69bb964514cfa387616f0d3a"
uuid = "2bd173c7-0d6d-553b-b6af-13a54713934c"
version = "1.3.0"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "85b5da0fa43588c75bb1ff986493443f821c70b7"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.3"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PooledArrays]]
deps = ["DataAPI", "Future"]
git-tree-sha1 = "db3a23166af8aebf4db5ef87ac5b00d36eb771e2"
uuid = "2dfb63ee-cc39-5dd5-95bd-886bf059d720"
version = "1.4.0"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "de893592a221142f3db370f48290e3a2ef39998f"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.2.4"

[[deps.PrettyTables]]
deps = ["Crayons", "Formatting", "Markdown", "Reexport", "Tables"]
git-tree-sha1 = "dfb54c4e414caa595a1f2ed759b160f5a3ddcba5"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "1.3.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Setfield]]
deps = ["ConstructionBase", "Future", "MacroTools", "Requires"]
git-tree-sha1 = "fca29e68c5062722b5b4435594c3d1ba557072a3"
uuid = "efcf1570-3423-57d1-acb7-fd33fddbac46"
version = "0.7.1"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.TableTraitsUtils]]
deps = ["DataValues", "IteratorInterfaceExtensions", "Missings", "TableTraits"]
git-tree-sha1 = "78fecfe140d7abb480b53a44f3f85b6aa373c293"
uuid = "382cd787-c1b6-5bf2-a167-d5b971a19bda"
version = "1.0.2"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.URIParser]]
deps = ["Unicode"]
git-tree-sha1 = "53a9f49546b8d2dd2e688d216421d050c9a31d0d"
uuid = "30578b45-9adc-5946-b283-645ec420af67"
version = "0.4.1"

[[deps.URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Vega]]
deps = ["DataStructures", "DataValues", "Dates", "FileIO", "FilePaths", "IteratorInterfaceExtensions", "JSON", "JSONSchema", "MacroTools", "NodeJS", "Pkg", "REPL", "Random", "Setfield", "TableTraits", "TableTraitsUtils", "URIParser"]
git-tree-sha1 = "43f83d3119a868874d18da6bca0f4b5b6aae53f7"
uuid = "239c3e63-733f-47ad-beb7-a12fde22c578"
version = "2.3.0"

[[deps.VegaLite]]
deps = ["Base64", "DataStructures", "DataValues", "Dates", "FileIO", "FilePaths", "IteratorInterfaceExtensions", "JSON", "MacroTools", "NodeJS", "Pkg", "REPL", "Random", "TableTraits", "TableTraitsUtils", "URIParser", "Vega"]
git-tree-sha1 = "3e23f28af36da21bfb4acef08b144f92ad205660"
uuid = "112f6efa-9a02-5b7d-90c0-432ed331239a"
version = "2.6.0"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╟─5d19678a-9e8d-11ec-0a86-f5436ecbfafb
# ╟─9cbff4ff-6142-4461-ae0b-f26749632475
# ╠═7ac5546a-fa2c-4589-aec1-b75e5ae05d46
# ╠═31a4298e-954b-44b9-b339-ec91195507c0
# ╟─515ba560-68e9-437b-8e7f-03c7f0a82aa6
# ╠═d85071e1-588d-4418-bee9-05d74dd15c72
# ╟─a316bfc5-d6bc-4140-bb5f-9aefbec8327a
# ╠═e4fec98b-7336-4086-be8d-5fd283e61fdb
# ╠═866b11e8-15df-43ef-972f-535d5d0d8f97
# ╟─e13aa492-b43f-4602-bbcf-609aa797072e
# ╠═f868a788-4e5e-4f3b-bc6e-1ea0d9947c7b
# ╠═065b53b2-54c2-40dc-b8a7-7c40020c108c
# ╟─12d813dd-01d4-48e7-8f40-32ddccc92451
# ╠═00e4419f-5218-4004-935a-54fcf4c46324
# ╠═831ec78b-2a33-498a-8f0d-76c5f3d80779
# ╠═2a0c52cd-49f6-45de-ae0f-63cd53612294
# ╠═83d9c4cf-87f6-4cb3-a6c3-71fb3c4ff41d
# ╠═77f06251-61f5-43f1-b949-7ab9ba885bea
# ╠═48ac13e2-9d0d-4807-931c-091b1e873c5d
# ╟─8ca327d6-83e9-498a-a793-c62c20db5c87
# ╟─1935bbd5-9235-4401-97ae-299aa5c01504
# ╟─99794994-da36-4646-afa0-66a5781c371c
# ╠═6a6ed95f-7cd3-40fe-a571-03d2eeb1a541
# ╟─ccacf398-faad-47ec-8f86-5fef1bdd43ca
# ╟─ff444df1-eeae-4d01-9824-60811a703408
# ╠═0ccee0aa-08e1-481c-8ce9-7274317de28b
# ╠═4cbeabe6-5121-4baf-9c33-66c7bdb3ad10
# ╟─47f3ad51-5c3a-4524-96ae-28786971c294
# ╠═5e07bdef-b750-4f17-9f77-f09fa17ed0ad
# ╟─24ef77f6-cb6b-49f9-acbb-e9f574db7f21
# ╠═85bf7690-25a6-436e-9795-cd2a600305af
# ╠═a6f29ff1-6b0d-41c1-9064-ed0bab3a4693
# ╟─f0b123d3-5b1d-4e11-9da2-1686cb2bef05
# ╟─c4f7fe77-5277-4ec2-8825-22c455741355
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
